<?php

namespace fakis\core\app;

use Yii;
use yii\base\Action;
use yii\helpers\Url;

/**
 * 应用模块特性
 * 主要使用在应用模块与原生应用的参数同步
 *
 * @property string $homeUrl 应用主页地址
 * @property string $runtimePath 执行环境目录路径
 * @property string $vendorPath
 *
 * @author Fakis <fakis738@qq.com>
 */
trait AppTrait
{
    /**
     * 应用模块名称
     * @var string
     */
    public $name = 'App Module';

    /**
     * 当前应用模块的字符编码
     * @var string
     */
    public $charset = 'UTF-8';

    /**
     * @var string
     */
    public $language = 'en-US';

    /**
     * @var string
     */
    public $sourceLanguage = 'en-US';

    /**
     * @var string
     */
    public $requestedRoute;

    /**
     * @var Action|null
     */
    public $requestedAction;

    /**
     * @var array
     */
    public $requestedParams;

    /**
     * @var array|null
     */
    public $extensions;

    /**
     * @var array
     */
    public $bootstrap = [];

    /**
     * @var int
     */
    public $state;

    /**
     * @var array
     */
    public $loadedModules = [];

    /**
     * @var array|null
     */
    public $catchAll;

    /**
     * @param string $path
     */
    public function setBasePath($path)
    {
        parent::setBasePath($path);
        Yii::setAlias('@package/basePath', $this->getBasePath());
    }

    private $_homeUrl;

    /**
     * 返回应用主页Url
     * @return string
     */
    public function getHomeUrl()
    {
        if ($this->_homeUrl === null) {
            return Url::to(['/' . $this->id], true);
        }

        return $this->_homeUrl;
    }

    /**
     * 设置应用主页Url
     * @param string $url
     */
    public function setHomeUrl($url)
    {
        $this->_homeUrl = $url;
    }

    private $_runtimePath;

    /**
     * @return string
     */
    public function getRuntimePath()
    {
        if ($this->_runtimePath === null) {
            $this->setRuntimePath($this->getBasePath() . DIRECTORY_SEPARATOR . 'runtime');
        }

        return $this->_runtimePath;
    }

    /**
     * @param string $path
     */
    public function setRuntimePath($path)
    {
        $this->_runtimePath = Yii::getAlias($path);
        Yii::setAlias('@package/runtime', $this->_runtimePath);
    }

    /**
     * 返回应用扩展目录路径
     * @return string
     */
    public function getVendorPath()
    {
        return Yii::$app->getVendorPath();
    }

    /**
     * 应用模块下不支持设置扩展目录路径
     * @param string $path
     */
    public function setVendorPath($path)
    {
    }

    /**
     * Returns the time zone used by this application.
     * This is a simple wrapper of PHP function date_default_timezone_get().
     * If time zone is not configured in php.ini or application config,
     * it will be set to UTC by default.
     * @return string the time zone used by this application.
     * @see https://www.php.net/manual/en/function.date-default-timezone-get.php
     */
    public function getTimeZone()
    {
        return date_default_timezone_get();
    }

    /**
     * Sets the time zone used by this application.
     * This is a simple wrapper of PHP function date_default_timezone_set().
     * Refer to the [php manual](https://www.php.net/manual/en/timezones.php) for available timezones.
     * @param string $value the time zone used by this application.
     * @see https://www.php.net/manual/en/function.date-default-timezone-set.php
     */
    public function setTimeZone($value)
    {
        date_default_timezone_set($value);
    }
}