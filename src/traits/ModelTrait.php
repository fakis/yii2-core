<?php

namespace fakis\core\traits;

use yii\base\InvalidConfigException;

/**
 * 重构模型特性
 *
 * 必须继承模型基类为 `\yii\base\Model`
 * 因为有太多的子类继承模型类，所以将需要重构的东西独立出来做为一个特性，给有需要的模型类来调用
 *
 * @property-read string|null $firstError
 *
 * @author Fakis <fakis738@qq.com>
 */
trait ModelTrait
{
    /**
     * 加载属性方法
     *
     * 可自动识别是否传来`formName`，解决每次需要传`$formName`的问题
     * - 在`web端`一般默认有`formName`
     * - 在`api端`一般没有传`formName`
     * ```
     * $model->load(Yii::$app->request->post())
     * ```
     *
     * @param array $data
     * @param string|null $formName
     * @return bool
     * @throws InvalidConfigException
     */
    public function load($data, $formName = null)
    {
        $data = (array)$data;
        $scope = $formName === null ? $this->formName() : $formName;
        return array_key_exists($scope, $data)
            ? parent::load($data, $formName)
            : parent::load($data, '');
    }

    /**
     * 返回第一条错误信息
     *
     * 可传空属性名，原框架默认必须传属性名
     * ```
     * $model->getFirstError()
     * ```
     *
     * @param string|null $attribute 属性名
     * @return string|null 返回错误消息，如果没有错误，则返回 `null`
     * @see getErrors()
     * @see getFirstErrors()
     */
    public function getFirstError($attribute = null)
    {
        if ($attribute !== null) {
            return parent::getFirstError($attribute);
        }

        if (!empty($this->firstErrors)) {
            $firstErrors = array_values($this->firstErrors);
            return $firstErrors[0] ?? null;
        }

        return null;
    }
}