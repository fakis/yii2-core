<?php

namespace fakis\core\helpers;

/**
 * Class Color
 *
 * @author Fakis <fakis738@qq.com>
 */
class Color
{
    /**
     * HSV转换RGB
     * @param float $h 色相（Hue），取值范围 0~360
     * @param float $s 饱和度（Saturation），取值范围 0~1
     * @param float $v 亮度（Value），取值范围 0~1
     * @return array
     */
    public static function hsv2rgb($h, $s, $v)
    {
        $rgb = [0, 0, 0];

        for ($i = 0; $i < 4; $i++) {
            if (abs($h - $i * 120) < 120) {
                $distance = max(60, abs($h - $i * 120));
                $rgb[$i % 3] = 1 - (($distance - 60) / 60);
            }
        }

        $max = max($rgb);
        $factor = 255 * $v;
        for ($i = 0; $i < 3; $i++) {
            $rgb[$i] = round(($rgb[$i] + ($max - $rgb[$i]) * (1 - $s)) * $factor);
        }

        return $rgb;
    }

    /**
     * RGB转换HEX
     * @param int $r
     * @param int $g
     * @param int $b
     * @return string
     */
    public static function rgb2hex($r, $g, $b)
    {
        return sprintf('#%02X%02X%02X', $r, $g, $b);
    }
}
