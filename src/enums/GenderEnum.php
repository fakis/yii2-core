<?php

namespace fakis\core\enums;

use fakis\core\base\Enum;

/**
 * 性别枚举
 *
 * @author Fakis <fakis738@qq.com>
 */
final class GenderEnum extends Enum
{
    public const UNKNOWN = 0;
    public const MALE = 1;
    public const FEMALE = 2;

    /**
     * 返回默认用例
     * @return array
     */
    public static function case(): array
    {
        return [
            self::MALE => '男',
            self::FEMALE => '女',
            self::UNKNOWN => '未知',
        ];
    }
}