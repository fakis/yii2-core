<?php

namespace fakis\core\enums;

use fakis\core\base\Enum;

/**
 * 是否枚举
 *
 * @author Fakis <fakis738@qq.com>
 */
final class WhetherEnum extends Enum
{
    public const YES = 1;
    public const NO = 0;

    /**
     * 返回默认用例
     * @return array
     */
    public static function case(): array
    {
        return [
            self::YES => '是',
            self::NO => '否',
        ];
    }

    /**
     * 是否启用
     * @return array
     */
    public static function caseActive(): array
    {
        return [
            self::YES => '启用',
            self::NO => '禁用',
        ];
    }
}