<?php

namespace fakis\core\enums;

use fakis\core\base\Enum;

/**
 * 排序枚举
 *
 * @author Fakis <fakis738@qq.com>
 */
final class SortEnum extends Enum
{
    public const ASC = 'asc';
    public const DESC = 'desc';

    /**
     * 返回默认用例
     * @return array
     */
    public static function case(): array
    {
        return [
            self::ASC => '升序',
            self::DESC => '降序',
        ];
    }
}