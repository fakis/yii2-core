<?php

namespace fakis\core\base;

use ArrayObject;
use fakis\core\traits\ModelAttributes;
use fakis\core\traits\ModelTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\validators\Validator;

/**
 * 定义模型
 *
 * @property DefineProp[] $props
 * @property-read array $defaultAttributes
 *
 * @author Fakis <fakis738@qq.com>
 */
class DefineModel extends \yii\base\Model
{
    use ModelTrait, ModelAttributes;

    /**
     * 定义属性
     * @var DefineProp[]
     */
    private $_props = [];

    /**
     * 返回所有定义属性
     * @return DefineProp[]
     */
    public function getProps()
    {
        return $this->_props;
    }

    /**
     * 批量设置定义属性
     * @param array|DefineProp[] $props
     * @throws InvalidConfigException
     */
    public function setProps(array $props)
    {
        foreach ($props as $name => $value) {
            if (is_int($name)) {
                if (is_string($value)) {
                    $this->setProp($value);
                } elseif (is_array($value) && !empty($value['name'])) {
                    $this->setProp($value['name'], $value);
                } elseif ($value instanceof DefineProp) {
                    $this->setProp($value->name, $value);
                }
            }
            if (is_string($name)) {
                $this->setProp($name, $value);
            }
        }
    }

    /**
     * 设置定义属性
     * @param string $name
     * @param mixed $value
     * @throws InvalidConfigException
     */
    public function setProp($name, $value = null)
    {
        $propClass = DefineProp::class;
        if (is_string($value)) {
            list($type, $default) = explode('|', $value);
            $prop = Yii::createObject(['class' => $propClass, 'name' => $name, 'type' => $type, 'default' => $default]);
        } elseif (is_array($value)) {
            $value['class'] = $propClass;
            $value['name'] = $name;
            $prop = Yii::createObject($value);
        } elseif ($value instanceof DefineProp) {
            $value->name = $name;
            $prop = $value;
        } else {
            $prop = Yii::createObject(['class' => $propClass, 'name' => $name]);
        }

        $this->_props[$name] = $prop;
        $this->_attributes[$name] = $prop->getDefaultValue();
    }

    /**
     * 返回定义属性
     * @param string $name
     * @return DefineProp
     */
    public function getProp($name)
    {
        return $this->_props[$name];
    }

    /**
     * 返回定义属性是否存在
     * @param string $name
     * @return bool
     */
    public function hasProp($name)
    {
        return array_key_exists($name, $this->getProps());
    }

    /**
     * 取得所有属性默认值
     * @return array
     */
    public function getDefaultAttributes()
    {
        $result = [];
        foreach ($this->getProps() as $prop) {
            $result[$prop->name] = $prop->getDefaultValue();
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function scenarios()
    {
        $scenarios = [static::SCENARIO_DEFAULT => []];
        foreach ($this->getProps() as $prop) {
            if (!$prop->readOnly) {
                $scenarios[static::SCENARIO_DEFAULT][] = $prop->name;
            }
        }
        return $scenarios;
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        $result = [];
        foreach ($this->getProps() as $prop) {
            if (is_string($prop->label)) {
                $result[$prop->name] = $prop->label;
            }
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function attributeHints()
    {
        $result = [];
        foreach ($this->getProps() as $prop) {
            if (is_string($prop->hints)) {
                $result[$prop->name] = $prop->hints;
            }
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function createValidators()
    {
        $validators = new ArrayObject();
        $validators->append(Validator::createValidator('safe', $this, $this->attributes()));

        foreach ($this->getProps() as $prop) {
            // 必填
            if ($prop->required) {
                $validators->append(Validator::createValidator('required', $this, $prop->name));
            }

            // 如果属性符合指定的类型，默认添加验证规则
            if (in_array($prop->type, [DefineProp::TYPE_STRING, DefineProp::TYPE_INTEGER, DefineProp::TYPE_NUMBER, DefineProp::TYPE_BOOLEAN], true)) {
                $validators->append(Validator::createValidator($prop->type, $this, $prop->name));
            }

            // 添加数组验证规则
            if ($prop->type === DefineProp::TYPE_ARRAY && !empty($prop->props)) {
                $validators->append(Validator::createValidator('validatePropArray', $this, $prop->name));
            }

            // 添加对象验证规则
            if ($prop->type === DefineProp::TYPE_OBJECT && !empty($prop->props)) {
                $validators->append(Validator::createValidator('validatePropObject', $this, $prop->name));
            }

            // 添加属性自定义规则
            foreach ($prop->rules as $rule) {
                if ($rule instanceof Validator) {
                    $validators->append($rule);
                } elseif (is_array($rule) && isset($rule[0])) {
                    $validator = Validator::createValidator($rule[0], $this, $prop->name, array_slice($rule, 1));
                    $validators->append($validator);
                } else {
                    throw new InvalidConfigException('Invalid validation rule: a rule must specify both attribute names and validator type.');
                }
            }
        }

        return $validators;
    }

    /**
     * 验证数组属性
     * @param string $attribute
     * @param mixed $params
     * @return void
     * @throws InvalidConfigException
     */
    public function validatePropArray($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $arrayOfValues = $this->$attribute;
            $props = $this->getProp($attribute)->props;
            if (is_array($arrayOfValues) && !empty($arrayOfValues) && !empty($props)) {
                foreach ($arrayOfValues as $index => $value) {
                    $model = new DefineModel($props);
                    $model->load($value) && $model->validate();
                    if (!empty($model->errors)) {
                        $errInfo = [];
                        foreach ($model->errors as $errors) {
                            foreach ($errors as $error) {
                                $index += 1;
                                $errInfo[] = "第{$index}条记录, {$error}";
                            }
                        }
                        $this->addErrors([$attribute => $errInfo]);
                        return;
                    }
                }
            }
        }
    }

    /**
     * 验证验证对象属性
     * @param string $attribute
     * @param mixed $params
     * @return void
     * @throws InvalidConfigException
     */
    public function validatePropObject($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $objectOfValues = $this->$attribute;
            $props = $this->getProp($attribute)->props;
            if (!empty($objectOfValues) && !empty($props)) {
                $model = new DefineModel($props);
                $model->load($objectOfValues) && $model->validate();
                if (!empty($model->errors)) {
                    $errInfo = [];
                    foreach ($model->errors as $errors) {
                        foreach ($errors as $error) {
                            $errInfo[] = $error;
                        }
                    }
                    $this->addErrors([$attribute => $errInfo]);
                    return;
                }
            }
        }
    }

    /**
     * 返回属性值
     * @param string|\Closure|array $key
     * @param mixed $default
     * @return mixed
     * @throws \Exception
     */
    public function getValue($key, $default = null)
    {
        return ArrayHelper::getValue($this, $key, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function __set($name, $value)
    {
        if ($this->hasAttribute($name)) {
            $this->_attributes[$name] = $this->getProp($name)->format($value);
        } else {
            parent::__set($name, $value);
        }
    }
}