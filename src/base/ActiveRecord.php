<?php

namespace fakis\core\base;

use fakis\core\traits\ModelDefine;
use fakis\core\traits\ModelTrait;

/**
 * ActiveRecord
 *
 * - [2022.9.1] 加入绑定定义模型功能
 *
 * @author Fakis <fakis738@qq.com>
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use ModelTrait, ModelDefine;

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->clearDefinedModels();
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritDoc
     */
    public function afterDelete()
    {
        $this->clearDefinedModels();
        parent::afterDelete();
    }
}