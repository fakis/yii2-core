<?php

namespace fakis\core\base;

use fakis\core\traits\ModelDefine;
use fakis\core\traits\ModelTrait;

/**
 * Model
 *
 * @author Fakis <fakis738@qq.com>
 */
class Model extends \yii\base\Model
{
    use ModelTrait;
}