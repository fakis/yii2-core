<?php

namespace fakis\core\web;

use fakis\core\base\Customizer;

/**
 * 通用控制器
 *
 * @author Fakis <fakis738@qq.com>
 */
class Controller extends \yii\web\Controller
{
    /**
     * 渲染Json格式数据
     * @param mixed $data
     * @param int $code
     * @return \yii\web\Response
     */
    public function renderJson($data, int $code = 0)
    {
        return $this->asJson((new Customizer())->format($data, $code));
    }

    /**
     * 渲染Xml格式数据
     * @param mixed $data
     * @param int $code
     * @return \yii\web\Response
     */
    public function renderXml($data, int $code = 0)
    {
        return $this->asXml((new Customizer())->format($data, $code));
    }
}