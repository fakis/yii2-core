<?php

namespace fakis\core\web;

use fakis\core\base\Customizer;
use Yii;

/**
 * @author Fakis <fakis738@qq.com>
 */
class ErrorHandler extends \yii\web\ErrorHandler
{
    /**
     * @param $exception
     * @return array
     */
    protected function convertExceptionToArray($exception)
    {
        return (new Customizer())->format($exception);
    }
}