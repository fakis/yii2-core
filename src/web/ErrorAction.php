<?php

namespace fakis\core\web;

use yii\base\Module;

class ErrorAction extends \yii\web\ErrorAction
{
    protected function getViewRenderParams()
    {
        $result = [
            'name' => $this->getExceptionName(),
            'message' => $this->getExceptionMessage(),
            'exception' => $this->exception,
            'app' => null,
        ];

        if (($module = $this->controller->module) instanceof \app\modules\a1\Module) {
            $result['app'] = [
                'id' => $module->id,
                'name' => $module->name,
                'logo' => $module->logo,
            ];
        }

        return $result;
    }
}