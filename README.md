# 扩展yii2框架的核心部件

[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist fakis/yii2-core "*"
```

or add

```
"fakis/yii2-core": "*"
```

to the require section of your `composer.json` file.


## 定义动态模型